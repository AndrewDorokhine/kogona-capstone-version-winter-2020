﻿using System.Collections.Generic;
using UnityEngine;

public static class Extensions
{
    /// <summary>
    /// Selects first from a list, or the default provided
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="source"></param>
    /// <param name="alternate"></param>
    /// <returns></returns>
    public static T FirstOr<T>(this IEnumerable<T> source, T alternate)
    {
        foreach (T t in source)
            return t;
        return alternate;
    }

    // Example usage of extension method to debug.log the name of an object with a prefix.
    //public static void GetName(this GameObject gameObject, string prefix)
    //{
    //    Debug.Log(prefix + gameObject.name);
    //}
}
