﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TrackGameUIAndScenes : MonoBehaviour
{
    [SerializeField] private Transform _startGame;
    [SerializeField] private Transform _endGame;
    [SerializeField] private Transform _pauseGame;
    [SerializeField] private BoolVariable _hasStartButtonBeenSeen;

    private const string TutorialHasPlayed = "TutorialHasPlayed";

    private void Start()
    {
        // Debug tool to view Tutorial on startup
        //PlayerPrefs.SetInt("TutorialHasPlayed", 0);
    }

    public void RemoveStart()
    {
        if (PlayerPrefs.GetInt(TutorialHasPlayed, 0) <= 0)
        {
            PlayerPrefs.SetInt(TutorialHasPlayed, 1);
            SceneManager.LoadScene(1);
        }
        else
        {
            _startGame.gameObject.SetActive(false);
        }
    }

    public void Loss()
    {
        _hasStartButtonBeenSeen.value = true;
        _endGame.gameObject.SetActive(true);
    }

    public void ReloadGame()
    {
        Time.timeScale = 1.0f;
        SceneManager.LoadScene(0);
    }

    public void ShowPauseMenu()
    {
        _pauseGame.gameObject.SetActive(true);
        // Load the menu scene async so it is ready whenever the player quits the game
    }

    public void RemovePauseMenu()
    {
        _pauseGame.gameObject.SetActive(false);
    }

    public void ReenableStartScreen()
    {
        _hasStartButtonBeenSeen.value = false;
    }
}

