﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorBehaviour : MonoBehaviour
{
    [SerializeField] private GameBoardBehaviour _gameBoard;

    public void TriggerColorChanging(GameObject square1, GameObject square2)
    {
        StartCoroutine(WaitToCallColorUpdate(square1, square2));
    }

    IEnumerator WaitToCallColorUpdate(GameObject square1, GameObject square2)
    {
        yield return null;
        ChangeColors(square1, square2);
    }

    private void ChangeColors(GameObject square1, GameObject square2)
    {
        square1.GetComponent<MeshRenderer>().material.color = _gameBoard.GetColorFromScriptableObject();
        square2.GetComponent<MeshRenderer>().material.color = _gameBoard.GetColorFromScriptableObject();
        UpdateColor(square1, square1.GetComponent<MeshRenderer>().material.color);
        UpdateColor(square2, square2.GetComponent<MeshRenderer>().material.color);

        // Removing guaranteed move as the randomizer has been implemented.
        // Keeping commented just in case.
        //if (!_getMoves.CheckMovesExistWholeBoard())
        //{
        //    StartCoroutine(_getMoves.GuaranteeMoveForTargetSquare(square2));
        //    UpdateColor(square1, square1.GetComponent<MeshRenderer>().material.color);
        //    UpdateColor(square2, square2.GetComponent<MeshRenderer>().material.color);
        //}
    }

    public void GetNewColor(GameObject go)
    {
        go.GetComponent<MeshRenderer>().material.color = _gameBoard.GetColorFromScriptableObject();
    }

    // Make it possible to update _currentColor without needing to make it public
    public void UpdateColor(GameObject square1, Color color)
    {
        square1.GetComponent<MeshRenderer>().material.color = color;
    }
}
