﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ButtonBehaviour : MonoBehaviour
{
    [SerializeField] private Button _randomizerButton;
    [SerializeField] private Button _pauseButton;
    [SerializeField] private Text _randomizerButtonText;

    [SerializeField] private Color _activeColor; // Text colour when randomizer is active
    [SerializeField] private Color _inactiveColor; // Text colour when randomizer is inactive

    private Scene _currentScene;

    private void Start()
    {
        _currentScene = SceneManager.GetActiveScene();
    }

    public void MakeButtonsActive()
    {
        _randomizerButton.gameObject.SetActive(true);
        _pauseButton.gameObject.SetActive(true);
    }

    public void ButtonOnCooldown()
    {
        _randomizerButton.interactable = false;
    }

    public void ButtonTextGreyOut()
    {
        _randomizerButtonText.color = _inactiveColor;
    }

    public void ButtonRecharged()
    {
        _randomizerButton.interactable = true;
        if (_currentScene.buildIndex == 0)
            _randomizerButtonText.color = _activeColor;
    }
}
