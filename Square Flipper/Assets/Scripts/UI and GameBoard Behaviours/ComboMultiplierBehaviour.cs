﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ComboMultiplierBehaviour : MonoBehaviour
{
    [SerializeField] private IntVariable _currentMultiplier;
	[SerializeField] private GameEvent _maxMultiplierReached;

    [SerializeField] private Text _multiplierText;
    [SerializeField] private float _multiplierResetTime;
    [SerializeField] private int _maximumMultiplier;

    [SerializeField] private ParticleSystem _stageParticles;

    private float _originalMultiplierResetTime;

    private bool _hasGameStarted;
	private bool _maintainMaxMultiplier = false;


    // Start is called before the first frame update
    void Start()
    {
        _currentMultiplier.value = 1;
        _originalMultiplierResetTime = _multiplierResetTime;
        _multiplierText.text = "";
        _hasGameStarted = false;
    }

    // Update is called once per frame
    public void Update()
    {
        if (_hasGameStarted)
        {
            _multiplierText.text = "x" + _currentMultiplier.value.ToString();
            _multiplierResetTime -= Time.deltaTime;

            if (_multiplierResetTime <= 0f)
            {
                _currentMultiplier.value = 1;
				_maintainMaxMultiplier = false;
            }
        }
        var main = _stageParticles.main;
        main.gravityModifier = (_currentMultiplier.value * 0.2f);

        var emit = _stageParticles.emission;
        emit.rateOverTime = (15 + (_currentMultiplier.value * 8));
    }

    public void GameStarted()
    {
        _hasGameStarted = true;
    }

    public void SquareMatchedMultiplier()
    {
        _multiplierResetTime = _originalMultiplierResetTime;
        if (_currentMultiplier.value < _maximumMultiplier)
        {
            _currentMultiplier.value += 1;
        }
        if (_currentMultiplier.value == _maximumMultiplier && !_maintainMaxMultiplier)
		{
			_maintainMaxMultiplier = true;
			_maxMultiplierReached.Raise();
		}
    }

    public void ResetMultOnRandomize()
    {
        _currentMultiplier.value = 1;
    }
}
