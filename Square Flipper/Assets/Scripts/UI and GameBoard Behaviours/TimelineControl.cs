﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.UI;

public class TimelineControl : MonoBehaviour
{
    [SerializeField] private PlayableDirector _timeline;

    [SerializeField] private GameEvent _firstPauseEvent;
    [SerializeField] private GameEvent _secondPauseEvent;
    [SerializeField] private GameEvent _disableTutorialDraggingEvent;

    // There are two separate timeline pauses because each raises a different event that different squares respond to.
    // This will keep squares from being flippable when they shouldn't be.
    public void FirstPausePlayback()
    {
        _timeline.Pause();
        _firstPauseEvent.Raise();
    }

    public void SecondPausePlayback()
    {
        _timeline.Pause();
        _secondPauseEvent.Raise();
    }

    public void ThirdPausePlayback()
    {
        _timeline.Pause();
    }

    public void ContinuePlayback()
    {
        _disableTutorialDraggingEvent.Raise();
        _timeline.Resume();
    }
}