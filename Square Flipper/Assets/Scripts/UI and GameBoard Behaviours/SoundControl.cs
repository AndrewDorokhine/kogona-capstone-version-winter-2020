﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SoundControl: MonoBehaviour
{
    [SerializeField] private AudioMixer _mixer;
    [SerializeField] private Text _musicText;
    [SerializeField] private Text _sfxText;

    [Space(10)]
    [SerializeField] private string _muteMusicText;
    [SerializeField] private string _unmuteMusicText;
    [SerializeField] private string _muteSFXText;
    [SerializeField] private string _unmuteSFXText;

    private const string MUSIC_KEY = "musicVol";
    private const string SFX_KEY = "sfxVol";
    private const string MUSIC_PARAM = "Music";
    private const string SFX_PARAM = "SFX";

    private float defaultMusicVol;
    private float defaultSFXVol;

    void Start()
    {
        defaultMusicVol = PlayerPrefs.GetFloat(MUSIC_KEY, 0);
        _mixer.SetFloat(MUSIC_PARAM, defaultMusicVol);
        if(defaultMusicVol == -80)
        {
            _musicText.text = _unmuteMusicText;
        }
        else
        {
            _musicText.text = _muteMusicText;
        }
        defaultSFXVol = PlayerPrefs.GetFloat(SFX_KEY, 0);
        _mixer.SetFloat(SFX_PARAM, defaultSFXVol);
        if (defaultSFXVol == -80)
        {
            _sfxText.text = _unmuteSFXText;
        }
        else
        {
            _sfxText.text = _muteSFXText;
        }
    }

    public void ToggleMusic()
    {
        _mixer.GetFloat(MUSIC_PARAM, out float curVol);
        if (curVol == -80)
        {
            _musicText.text = _muteMusicText;
            _mixer.SetFloat(MUSIC_PARAM, 0);
            PlayerPrefs.SetFloat(MUSIC_KEY, 0);
        }
        else
        {
            _musicText.text = _unmuteMusicText;
            _mixer.SetFloat(MUSIC_PARAM, -80);
            PlayerPrefs.SetFloat(MUSIC_KEY, -80);
        }
    }

    public void ToggleSFX()
    {
        _mixer.GetFloat(SFX_PARAM, out float curVol);
        if (curVol == -80)
        {
            _sfxText.text = _muteSFXText;
            _mixer.SetFloat(SFX_PARAM, 0);
            PlayerPrefs.SetFloat(SFX_KEY, 0);
        }
        else
        {
            _sfxText.text = _unmuteSFXText;
            _mixer.SetFloat(SFX_PARAM, -80);
            PlayerPrefs.SetFloat(SFX_KEY, -80);
        }
    }
}
