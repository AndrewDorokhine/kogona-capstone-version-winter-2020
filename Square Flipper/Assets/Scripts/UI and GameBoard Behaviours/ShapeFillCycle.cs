﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShapeFillCycle : MonoBehaviour
{
    [SerializeField] private float _animationSpeed;
    private Image _shape;

    // Start is called before the first frame update
    void Start()
    {
        _shape = gameObject.GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        _shape.fillAmount += Time.deltaTime * _animationSpeed;
        if(_shape.fillAmount >= 1)
        {
            _shape.fillAmount = 0;
        }
    }
}
