﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ScoreBehaviour : MonoBehaviour
{
    [SerializeField] private FloatVariable _timeSurvived;
    [SerializeField] private FloatVariable _highScore;

    [SerializeField] private Text _gameOverText;
    [SerializeField] private Text _endHiScoreText;
    [SerializeField] private Text _scoreThisTimeText;
    [SerializeField] private Text _personalBestText;
    [SerializeField] private Text _endgameQuote;

    [SerializeField] private string[] _endQuotes;

    [Space(10)]
    // Changes these strings to inspector fields so they can be set manually
    // Helps for future localization
    [SerializeField] private string _gameOverMessage;
    [SerializeField] private string _newHighScoreMessage;
    [SerializeField] private string _personalBestMessage;
    [SerializeField] private string _timeUnit; //Time unit in the game over screen, e.g. "seconds." Can be changed for other languages.


    private const string HIGH_SCORE_KEY = "hiScore";
    //private float _highScore;

    // Start is called before the first frame update
    void Start()
    {
        _timeSurvived.value = 0;
        _highScore.value = PlayerPrefs.GetFloat(HIGH_SCORE_KEY);

        _personalBestText.text = "";
        _scoreThisTimeText.text = "";
        _endHiScoreText.text = "";
        _gameOverText.text = _gameOverMessage;
    }

    public void OnGameLoss()
    {
        _endgameQuote.text = _endQuotes[Random.Range(0, _endQuotes.Length)];
        //PlayerPrefs.SetFloat(HIGH_SCORE_KEY, 3); // Reset highscore for testing.
        if (_timeSurvived.value > _highScore.value)
        {
            PlayerPrefs.SetFloat(HIGH_SCORE_KEY, _timeSurvived.value);
            _gameOverText.text = _newHighScoreMessage;
            _scoreThisTimeText.text = _timeSurvived.value.ToString("F2") + " " + _timeUnit;
        }
        else
        {
            _scoreThisTimeText.text = _timeSurvived.value.ToString("F2") + " " + _timeUnit;
            _endHiScoreText.text = _highScore.value.ToString("F2") + " " + _timeUnit;
            _personalBestText.text = _personalBestMessage;
        }
    }
}

