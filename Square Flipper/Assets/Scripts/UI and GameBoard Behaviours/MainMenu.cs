﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class MainMenu : MonoBehaviour
{
    [SerializeField] BoolVariable _hasStartButtonBeenSeen;

    private const string boardDimensionX = "boardX";
    private const string boardDimensionY = "boardY";
    private const string TutorialHasPlayed = "TutorialHasPlayed";

    public void StartGame(Vector2Int dimensions)
    {
        PlayerPrefs.SetInt(boardDimensionX, dimensions.x);
        PlayerPrefs.SetInt(boardDimensionY, dimensions.y);

        if (PlayerPrefs.GetInt(TutorialHasPlayed, 0) <= 0)
        {
            PlayerPrefs.SetInt(TutorialHasPlayed, 1);
            SceneManager.LoadScene(1);
        }
        else
        {
            SceneManager.LoadScene(0);
        }
    }

    public void StartTutorial()
    {
        SceneManager.LoadScene(1);
    }

    // Resets the "TutorialHasPlayed" playerpref so it can be viewed again. For testing only.
    public void ResetTutorial()
    {
        PlayerPrefs.SetInt(TutorialHasPlayed, 0);
    }

}
