﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

public class MovesExist : MonoBehaviour
{
    //[SerializeField] private GameObject _randomizerPowerup;
    [SerializeField] private GameObject[] _powerUpList;
    [SerializeField] private GameEventGameObjectAndColor _colorUpdateEvent;
    [SerializeField] private GameEventGameObject _wiggleRandomSquaresEvent;
    [SerializeField] private GameEvent _buttonRechargedEvent;
    [SerializeField] private Image _buttonRefillMeter;
    [SerializeField] private int _maxNumSquaresToRandomize;
    [SerializeField] private float _randomizerButtonCooldown;
    private float _buttonCooldown = 0f;
    [SerializeField] private BoolVariable _isButtonUsable;

    private GameObject[] _squareList;
    private GameObject[] _neighbourlessSquares = new GameObject[16];
    private HashSet<GameObject> _subSquareList;

    private bool moveExists;

    // Added for the tutorial, not needed for actual game
    void Start()
    {
        GetSquaresOnStartup();
    }

    // Keeps track of the randomizer button's cooldown
    void Update()
    {
        _buttonCooldown += Time.deltaTime;
        _buttonRefillMeter.fillAmount = _buttonCooldown / _randomizerButtonCooldown;
        if (_buttonCooldown > _randomizerButtonCooldown)
        {
            _buttonRechargedEvent.Raise();
        }
    }

    public void GetSquaresOnStartup()
    {
        _squareList = GameObject.FindGameObjectsWithTag("Square");
        moveExists = false;

        foreach (GameObject square in _squareList)
        {
            _colorUpdateEvent.Raise(square, square.GetComponent<MeshRenderer>().material.color);
        }

        StartCoroutine(Initialize());
    }

    public void SpawnNewRandomizerIcon()
    {
        GameObject powerUp = _powerUpList[Random.Range(0, _powerUpList.Length)];
        GameObject iconParent = _squareList[Random.Range(0, _squareList.Length)];
        Instantiate(powerUp, iconParent.transform);
    }

    public void RandomizeBoard()
    {
        if (_isButtonUsable.value)
        {
            _buttonCooldown = 0f;
            _neighbourlessSquares = new GameObject[16];
            _subSquareList = new HashSet<GameObject>();

            foreach (GameObject square in _squareList)
            {
                if (!CheckMovesExistForTargetSquare(square))
                {
                    _subSquareList.Add(square);
                }
            }
            _neighbourlessSquares = _subSquareList.ToArray();

            for (int i = 0; i < _maxNumSquaresToRandomize; i++)
            {
                // Trying to use the randomize button when there aren't any neighborless squares causes an error.
                // Try/catch will make the button do nothing if no neighborless squares exist.
                try
                {
                    GameObject square = _neighbourlessSquares[Random.Range(0, _neighbourlessSquares.Length)];

                    if (square) // Null check
                    {
                        if (!CheckMovesExistForTargetSquare(square))
                        {
                            StartCoroutine(GuaranteeMoveForTargetSquare(square));
                            _wiggleRandomSquaresEvent.Raise(square);
                        }
                    }
                }
                catch
                {
                    return;
                }
            }
        }
    }

    public void ResetMovesExist()
    {
        moveExists = false;
    }

    public bool DoesMoveExist()
    {
        return moveExists;
    }

    public IEnumerator Initialize()
    {
        yield return null;
        moveExists = CheckMovesExistWholeBoard();

        if (!moveExists)
        { 
            StartCoroutine(GuaranteeMoveWholeBoard());
        }
    }

    public void MakeBoardSameColor()
    {
            StartCoroutine(BoardSameColor());
    }

    IEnumerator BoardSameColor()
    {
        yield return null;

        //Get a random square from the list to grab the color of
        GameObject chosenSquare = _squareList[Random.Range(0, _squareList.Length)];
        Color chosenColor = chosenSquare.GetComponent<MeshRenderer>().material.color;

        foreach (GameObject square in _squareList)
        {
            ChangeBoardToSameColor(square, chosenColor);
            _wiggleRandomSquaresEvent.Raise(square);
        }
    }

    /* Iterating over each square on the board, find the ones adjacent to it and check if their colours are the same.
     * If they're not the same and no valid move exists (checks are made before GuaranteeMove() is called), recolour them to create a valid move.
     */
    IEnumerator GuaranteeMoveWholeBoard()
    {
        yield return null;

        // Pick out a random square from the list, so we aren't guaranteeing moves predictably.
        GameObject selectedSquare = _squareList[Random.Range(0, _squareList.Length)];

        foreach (GameObject square in _squareList)
        {
            if(ChangeColorToGuaranteeMove(selectedSquare, square))
            {
                break;
            }
        }
    }


    public IEnumerator GuaranteeMoveForTargetSquare(GameObject targetSquare)
    {
        yield return null;

        foreach (GameObject square in _squareList)
        {
            if (ChangeColorToGuaranteeMove(targetSquare, square))
            {
                break;
            }
        }
    }

    /* Iterating over each square on the board, find the ones adjacent to it and check if their colours are the same.
     * If they are, return true - there is a valid move on the board.
     */
    public bool CheckMovesExistWholeBoard()
    {
        foreach (GameObject square1 in _squareList)
        {
            foreach (GameObject square2 in _squareList)
            {
                if(CompareTwoSquareColors(square1, square2))
                {
                    return true;
                }
            }
        }
        return false;
    }

    // Required for the randomizer to know which squares to change the colours of.
    public bool CheckMovesExistForTargetSquare(GameObject targetSquare)
    {
        foreach (GameObject square in _squareList)
        {
            if(CompareTwoSquareColors(targetSquare, square))
            {
                return true;
            }
        }
        return false;
    }

    private void ChangeBoardToSameColor(GameObject square, Color color)
    {
        square.GetComponent<MeshRenderer>().material.color = color;
        _colorUpdateEvent.Raise(square, square.GetComponent<MeshRenderer>().material.color);
    }

    public void BombPowerupColorChange(GameObject powerUp)
    {
        GameObject tappedSquare = powerUp.transform.parent.gameObject;
        StartCoroutine(SameColorInLine(tappedSquare));
    }

    IEnumerator SameColorInLine(GameObject tappedSquare)
    {
        yield return null;

        Color tappedSquareColor = tappedSquare.GetComponent<MeshRenderer>().material.color;

        foreach (GameObject square in _squareList)
        {
            if (square.transform.position.x == tappedSquare.transform.position.x || square.transform.position.y == tappedSquare.transform.position.y)
            {
                ChangeBoardToSameColor(square, tappedSquareColor);
                _wiggleRandomSquaresEvent.Raise(square);
            }
        }
    }

    // Helper function for the GuaranteeMove coroutines.
    private bool ChangeColorToGuaranteeMove(GameObject square1, GameObject square2)
    {
        Color square1Color = square1.GetComponent<MeshRenderer>().material.color;
        Color square2Color = square2.GetComponent<MeshRenderer>().material.color;

        // An adjacent square can be defined as a square with either: The same x position but different y positions, or
        // The same y position but different x positions.
        // This may need to change if the board becomes larger: E.x. the same x position but a y position with an offset of one square away? 
        if (square1.transform.position.x == square2.transform.position.x && (square1.transform.position.y + 1) == square2.transform.position.y)
        {
            if (!(square1Color == square2Color))
            {
                square1.GetComponent<MeshRenderer>().material.color = square2Color;
                _colorUpdateEvent.Raise(square1, square1.GetComponent<MeshRenderer>().material.color);
                return true;
            }
        }
        else if (square1.transform.position.y == square2.transform.position.y && (square1.transform.position.x + 1) == square2.transform.position.x)
        {
            if (!(square1Color == square2Color))
            {
                square1.GetComponent<MeshRenderer>().material.color = square2Color;
                _colorUpdateEvent.Raise(square1, square1.GetComponent<MeshRenderer>().material.color);
                return true;
            }
        }
        else if (square1.transform.position.x == square2.transform.position.x && (square1.transform.position.y - 1) == square2.transform.position.y)
        {
            if (!(square1Color == square2Color))
            {
                square1.GetComponent<MeshRenderer>().material.color = square2Color;
                _colorUpdateEvent.Raise(square1, square1.GetComponent<MeshRenderer>().material.color);
                return true;
            }
        }
        else if (square1.transform.position.y == square2.transform.position.y && (square1.transform.position.x - 1) == square2.transform.position.x)
        {
            if (!(square1Color == square2Color))
            {
                square1.GetComponent<MeshRenderer>().material.color = square2Color;
                _colorUpdateEvent.Raise(square1, square1.GetComponent<MeshRenderer>().material.color);
                return true;
            }
        }
        return false;
    }

    // Helper function for checking if moves exist on the board or for a given square.
    private bool CompareTwoSquareColors(GameObject square1, GameObject square2)
    {
        Color square1Color = square1.GetComponent<MeshRenderer>().material.color;
        Color square2Color = square2.GetComponent<MeshRenderer>().material.color;

        if (!moveExists)
        {
            // An adjacent square can be define as a square with either: The same x position but different y positions, or
            // The same y position but different x positions.
            if (square1.transform.position.x == square2.transform.position.x && (square1.transform.position.y + 1) == square2.transform.position.y)
            {
                if (square1Color == square2Color)
                {
                    return true;
                }
            }
            else if (square1.transform.position.y == square2.transform.position.y && (square1.transform.position.x + 1) == square2.transform.position.x)
            {
                if (square1Color == square2Color)
                {
                    return true;
                }
            }
            else if (square1.transform.position.x == square2.transform.position.x && (square1.transform.position.y - 1) == square2.transform.position.y)
            {
                if (square1Color == square2Color)
                {
                    return true;
                }
            }
            else if (square1.transform.position.y == square2.transform.position.y && (square1.transform.position.x - 1) == square2.transform.position.x)
            {
                if (square1Color == square2Color)
                {
                    return true;
                }
            }
        }
        return false;
    }
}
