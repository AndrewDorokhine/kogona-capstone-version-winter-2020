﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DOTweeningBehaviour : MonoBehaviour
{
    [SerializeField] private float _shakeAmount; //Set to 1.2 for most of development but new squares need more.
    [SerializeField] private int strength;
    [SerializeField] private int vibrato;
    [SerializeField] private float duration;

    public void SquareWiggler(GameObject go1, GameObject go2)
    {
        go1.GetComponent<MoveSquare>().RunWiggleTween(_shakeAmount, strength, vibrato, duration);
        go2.GetComponent<MoveSquare>().RunWiggleTween(_shakeAmount, strength, vibrato, duration);
    }

    // Wiggles only one square at a time rather than the two involved in a match. Used for the randomizer so each affected square wiggles.
    // Resets scale afterwards to help handle a bug where squares would sometimes be larger after the PunchScale ended.
    public void WiggleOneSquare(GameObject go)
    {
        go.GetComponent<MoveSquare>().RunWiggleTween(_shakeAmount, strength, vibrato, duration);
    }
}
