﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonPressSoundDontDestroyOnLoad : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);

        // Destroys extra ButtonClicked objects so there isn't more than one in the scene.
        // Allows ButtonClicked to persist between scenes so the button sound isn't cut off by SceneLoad.
        if (FindObjectsOfType(GetType()).Length > 1)
        {
            Destroy(gameObject);
        }
    }
}
