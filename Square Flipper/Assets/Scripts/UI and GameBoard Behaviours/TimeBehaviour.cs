﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TimeBehaviour : MonoBehaviour
{
    //[SerializeField] private Button _startButton;
    [SerializeField] private Transform _startScreen;
    [SerializeField] private Transform _countDownScreen;
    [SerializeField] private GameEvent _playCountDownBeepEvent;
    [SerializeField] private GameEvent _FinishingCountDownBeepEvent;
    [SerializeField] private GameEvent _startButtonPressedEvent;
    [SerializeField] private GameEvent _gameStartEvent;
    [SerializeField] private GameEvent _gameEndEvent;
    [SerializeField] private GameEvent _timeRunningOutEvent;
    [SerializeField] private GameEvent _timeNoLongerLowEvent;
    //[SerializeField] private GameEvent _highScoreReachedEvent;
    [SerializeField] private GameEvent _spawnNewRandomizerEvent;

    [SerializeField] private FloatVariable _timeSurvived;
    [SerializeField] private IntVariable _multiplier; // Set to 1 by default
    [SerializeField] private BoolVariable _hasStartButtonBeenSeen;
    [SerializeField] private FloatVariable _highScore; // Used to track the highScoreBar's fill.

    [SerializeField] private Image _timerBarRight;
    [SerializeField] private Image _timerBarLeft;
    // Shows the time left until a player beats their high score as a secondary bar above the main one.
    [SerializeField] private Image _highScoreBarRight;
    [SerializeField] private Image _highScoreBarLeft;

    [SerializeField] private Text _waitTimerText;
    [SerializeField] private Text _timeSurvivedText;
    [SerializeField] private Text _newHighScoreText;

    [SerializeField] private float _waitTimer;
    [SerializeField] private float _currentGameTime;
    [SerializeField] private float _pointOfLowTime;
    [SerializeField] private float _timeIncreaseOnMatch;
    [SerializeField] private float _timeIntervalBetweenPowerups;
    [SerializeField] private float _windZoneLifespan;

    [Space(10)]
    [SerializeField] private string _startGameMessage1;
    [SerializeField] private string _startGameMessage2;
    [SerializeField] private string _startGameMessage3;
    [SerializeField] private string _newHighScoreDuringGameMessage;

    [SerializeField] private WindZone _sphereWindZone;

    private float _fullGameTime;
    private float _timeLeftToBeatHighScore;
    private float _timeSinceLastRandomizerSpawn;
    private float _timeUntilWindZoneDisabled;

    private bool _hasGameStarted;
    private bool _warningSoundPlaying = false;
    private bool _countDownStarted = false;
    private bool _countDownRunning = false;
    private bool _gameOverSoundPlayed = false;
    private bool _isTrackingTime = false;

    private const string HIGH_SCORE_KEY = "hiScore";
    private const string CountdownTickSound = "CountDownTick";

    // Start is called before the first frame update
    void Start()
    {
        _highScore.value = PlayerPrefs.GetFloat(HIGH_SCORE_KEY);
        _hasGameStarted = false;

        if (!_hasStartButtonBeenSeen.value)
        {
            //_startButton.gameObject.SetActive(true);
            _startScreen.gameObject.SetActive(true);
        }
        if (_hasStartButtonBeenSeen.value)
        {
            Time.timeScale = 1.0f;
            _startButtonPressedEvent.Raise();
        }

        _hasStartButtonBeenSeen.value = false;

        Time.timeScale = 1.0f;
        _timeSurvivedText.text = "";
        _timeSurvived.value = 0.0f;

        _fullGameTime = _currentGameTime;
        _timeLeftToBeatHighScore = _highScore.value;

        _newHighScoreText.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        if (_waitTimer > 0 &&  _countDownStarted)
        {
            RemoveStart();
        }
        if (_hasGameStarted)
        {
            if (_isTrackingTime)
            {
            _timeSurvived.value += Time.deltaTime;
            }

            _timerBarRight.gameObject.SetActive(true);
            _timerBarLeft.gameObject.SetActive(true);
            _highScoreBarRight.gameObject.SetActive(true);
            _highScoreBarLeft.gameObject.SetActive(true);

            _timeSinceLastRandomizerSpawn += Time.deltaTime;

            _currentGameTime -= Time.deltaTime;
            if(_currentGameTime > _fullGameTime)
            {
                _fullGameTime = _currentGameTime;
            }
            float currentFill = _currentGameTime / _fullGameTime;
            
            _timerBarRight.fillAmount = currentFill;
            _timerBarLeft.fillAmount = currentFill;

            _timeLeftToBeatHighScore -= Time.deltaTime;
            float highScoreFill = _timeLeftToBeatHighScore / _highScore.value;

            _highScoreBarRight.fillAmount = highScoreFill;
            _highScoreBarLeft.fillAmount = highScoreFill;
        }

        if(_currentGameTime > _pointOfLowTime && _warningSoundPlaying)
        {
            _warningSoundPlaying = false;
            _timeNoLongerLowEvent.Raise();
        }

        if (_currentGameTime <= _pointOfLowTime && !_warningSoundPlaying)
        {
            _warningSoundPlaying = true;
            _timeRunningOutEvent.Raise();
        }

        if (_currentGameTime <= 0f)
        {
            if (!_gameOverSoundPlayed)
            {
                _gameEndEvent.Raise();
                _gameOverSoundPlayed = true;
            }
        }

        if (_timeLeftToBeatHighScore < 0)
        {
            //_highScoreReachedEvent.Raise();
            _highScoreBarRight.gameObject.SetActive(true);
            _highScoreBarLeft.gameObject.SetActive(true);
            _newHighScoreText.text = _newHighScoreDuringGameMessage;
        }

        if (_timeSinceLastRandomizerSpawn > _timeIntervalBetweenPowerups)
        {
            _timeSinceLastRandomizerSpawn = 0;
            _spawnNewRandomizerEvent.Raise();
        }

        if (_sphereWindZone.gameObject.activeSelf)
        {
            _timeUntilWindZoneDisabled += Time.deltaTime;
            if (_timeUntilWindZoneDisabled >= _windZoneLifespan)
            {
                _timeUntilWindZoneDisabled = 0;
                _sphereWindZone.gameObject.SetActive(false);
            }
        }
    }

    public void RemoveStart()
    {
        _waitTimer -= Time.deltaTime;
        if (!_countDownRunning)
        {
            InvokeRepeating(CountdownTickSound, 0.0f, 0.9f);
            _countDownRunning = true;
        }
        //_waitTimerText.text = _waitTimer.ToString("F0");
        if (_waitTimer > 2)
        {
            _waitTimerText.text = _startGameMessage1;
        }
        else if (_waitTimer > 1)
        {
            _waitTimerText.text = _startGameMessage2;
        }
        else if (_waitTimer > 0)
        {
            _waitTimerText.text = _startGameMessage3;
        }
        else if (_waitTimer <= 0f)
        {
            _hasGameStarted = true;
            _gameStartEvent.Raise();
            _isTrackingTime = true;
        }
    }

    void CountDownTick()
    {
        if (_waitTimer > 1)
            _playCountDownBeepEvent.Raise();
        else
        {
            _FinishingCountDownBeepEvent.Raise();
            CancelInvoke(CountdownTickSound);
        }
    }

    public void SquareMatchedTimer()
    {
        _currentGameTime += _timeIncreaseOnMatch * _multiplier.value;
    }

    public void Loss()
    {
        _isTrackingTime = false;
        //Time.timeScale = 0.0f;
    }

    public void PauseGame()
    {
        Time.timeScale = 0.0f;
    }

    public void UnpauseGame()
    {
        Time.timeScale = 1.0f;
    }

    public void StartButtonPressed()
    {
        Time.timeScale = 1.0f;
        _countDownStarted = true;
    }

    public void StartGame()
    {
        _countDownScreen.gameObject.SetActive(false);
    }
}
