﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using RoboRyanTron.QuickButtons;

public class GameBoardBehaviour : MonoBehaviour
{
    [SerializeField] private ColorPalette _colorPalette;
    [SerializeField] private Vector2Int _boardDimensions = default;

    [SerializeField] private bool isTesting; // When this is enabled, the inspector variables will be used to make the board instead of playerprefs

    [SerializeField] private GameObject _gameBoardTopSide;
    [SerializeField] private GameObject _gameBoardBottomSide;
    [SerializeField] private GameObject _gameBoardRightSide;
    [SerializeField] private GameObject _gameBoardLeftSide;

    [SerializeField] private GameObject _squarePrefab;

    [SerializeField] private GameEvent _boardFinishedBuildingEvent;

    private GameBoard _gameBoard;

    void Start()
    {
        Screen.fullScreen = false; //Added to be able to play in window on PC for capstone Demo
        Screen.SetResolution(640, 1000, true);

        //_gameBoardTopSide.GetName("Senor "); // Example usage of an extension method to debug log an object's name with a prefix.
        if (!isTesting)
        {
            _boardDimensions.x = PlayerPrefs.GetInt("boardX");
            _boardDimensions.y = PlayerPrefs.GetInt("boardY");
        }

        _gameBoardTopSide.transform.position = new Vector3((_boardDimensions.x / 2) - 1, (_boardDimensions.y) - 1, 0);
        _gameBoardBottomSide.transform.position = new Vector3((_boardDimensions.x / 2) - 1, -1, 0);
        _gameBoardRightSide.transform.position = new Vector3((_boardDimensions.x) - 1, (_boardDimensions.y / 2) - 1, 0);
        _gameBoardLeftSide.transform.position = new Vector3(-1, (_boardDimensions.y / 2) - 1, 0);

        _gameBoard = new GameBoard(_boardDimensions.x, _boardDimensions.y, _colorPalette);

        foreach (GameSquare square in _gameBoard.Squares)
        {
            GameObject go = Instantiate(_squarePrefab, transform);
            go.name = $"{square.X}, {square.Y}";
            go.transform.position = new Vector3(square.X, square.Y, 0);
            go.GetComponentInChildren<MeshRenderer>().material.color = square.Color;

            go.GetComponentInChildren<MoveSquare>().setDefaultValidRotations
                (square.draggableRight, square.draggableUp, square.draggableLeft, square.draggableDown);
        }

        _boardFinishedBuildingEvent.Raise();
    }

    public Color GetColorFromScriptableObject()
    {
        int newColorIndex = UnityEngine.Random.Range(0, _colorPalette.Colors.Count);
        return _colorPalette.Colors[newColorIndex];
    }
}