﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PowerupBehaviour : MonoBehaviour
{
    public void DestroyIconAfterTouch()
    {
        Destroy(gameObject);
    }

    public void TransferIconToNewParent(GameObject newParent)
    {
        Instantiate(gameObject, newParent.transform);
        Destroy(gameObject);
    }
}
