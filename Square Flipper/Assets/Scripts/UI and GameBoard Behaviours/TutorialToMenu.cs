﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TutorialToMenu : MonoBehaviour
{
    [SerializeField] BoolVariable _hasStartButtonBeenSeen;

    void Start()
    {
        _hasStartButtonBeenSeen.value = true;
    }

    public void ReplayTutorial()
    {
        SceneManager.LoadScene(1);
    }

    public void StartGame()
    {
        SceneManager.LoadScene(0);
    }
}
