﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialRandomizer : MonoBehaviour
{
    [SerializeField] private GameEvent _buttonRechargedEvent;
    [SerializeField] private Image _buttonRefillMeter;
    [SerializeField] private GameObject _bottomLeft;
    [SerializeField] private GameObject _bottomRight;
    [SerializeField] private GameObject _topLeft;
    [SerializeField] private GameObject _topRight;
    [SerializeField] private GameEventGameObject _wiggleRandomSquaresEvent;
    [SerializeField] private float _randomizerButtonCooldown;

    private float _buttonCooldown = 0f;
    private bool _buttonHasBeenClicked = false;

    void Update()
    {
        if (_buttonHasBeenClicked)
        {
            _buttonCooldown += Time.deltaTime;
        }

        _buttonRefillMeter.fillAmount = _buttonCooldown / _randomizerButtonCooldown;

        if (_buttonCooldown > _randomizerButtonCooldown)
        {
            _buttonRechargedEvent.Raise();
        }
    }

    public void RandomizeBoard()
    {
        _buttonCooldown = 0f;
        _buttonHasBeenClicked = true;
        Color _demoColor1 = _bottomLeft.GetComponent<MeshRenderer>().material.color;
        Color _demoColor2 = _bottomRight.GetComponent<MeshRenderer>().material.color;
        Color _demoColor3 = _topLeft.GetComponent<MeshRenderer>().material.color;
        Color _demoColor4 = _topRight.GetComponent<MeshRenderer>().material.color;

        if (_demoColor1 != _demoColor2)
        {
            _bottomLeft.GetComponent<MeshRenderer>().material.color = _demoColor2;
            _wiggleRandomSquaresEvent.Raise(_bottomLeft);
        }
        if (_demoColor3 != _demoColor4)
        {
            _topLeft.GetComponent<MeshRenderer>().material.color = _demoColor4;
            _wiggleRandomSquaresEvent.Raise(_topLeft);
        }
    }

    public void StopTrackingButtonCooldown()
    {
        _buttonHasBeenClicked = false;
    }
}
