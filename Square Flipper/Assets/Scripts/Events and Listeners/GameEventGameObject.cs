﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New GameObject Event", menuName = "Game Events/New GameObject Event")]
public class GameEventGameObject : ScriptableObject
{
    private List<GameEventListenerGameObject> _registeredListeners = new List<GameEventListenerGameObject>();

    public void Raise(GameObject go1)
    {
        foreach (GameEventListenerGameObject listener in _registeredListeners)
        {
            listener.Raise(go1);
        }
    }

    public void RegisterListener(GameEventListenerGameObject listener)
    {
        if (!_registeredListeners.Contains(listener))
            _registeredListeners.Add(listener);
    }

    public void UnregisterListener(GameEventListenerGameObject listener)
    {
        if (_registeredListeners.Contains(listener))
            _registeredListeners.Remove(listener);
    }
}
