﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEventListenerGameObject : MonoBehaviour
{
    [SerializeField] private GameEventGameObject _gameEvent;
    [SerializeField] private UnityEventGameObject _onInvoke;

    private void OnEnable()
    {
        _gameEvent.RegisterListener(this);
    }

    private void OnDisable()
    {
        _gameEvent.UnregisterListener(this);
    }

    public void Raise(GameObject go1)
    {
        if (_onInvoke != null)
            _onInvoke.Invoke(go1);
    }
}
