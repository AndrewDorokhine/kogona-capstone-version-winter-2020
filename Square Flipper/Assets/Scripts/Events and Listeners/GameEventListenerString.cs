﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEventListenerString : MonoBehaviour
{
    [SerializeField] private GameEventString _gameEvent;
    [SerializeField] private UnityEventString _onInvoke;

    private void OnEnable()
    {
        _gameEvent.RegisterListener(this);
    }

    private void OnDisable()
    {
        _gameEvent.UnregisterListener(this);
    }

    public void Raise(string name)
    {
        if (_onInvoke != null)
            _onInvoke.Invoke(name);
    }
}
