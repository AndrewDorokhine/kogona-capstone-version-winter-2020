﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class GameEventVector2IntListener : MonoBehaviour
{
    [SerializeField] private GameEventVector2Int _gameEvent;
    [SerializeField] private UnityEventVector2Int _onInvoke;

    private void OnEnable()
    {
        _gameEvent.RegisterListener(this);
    }

    private void OnDisable()
    {
        _gameEvent.UnregisterListener(this);
    }

    public void Raise(Vector2Int dimensions)
    {
        if (_onInvoke != null)
            _onInvoke.Invoke(dimensions);
    }

    public void StartGame(Vector2Int dimensions)
    {
        PlayerPrefs.SetInt("boardX", dimensions.x);
        PlayerPrefs.SetInt("boardY", dimensions.y);

        SceneManager.LoadScene(1);
    }
}
