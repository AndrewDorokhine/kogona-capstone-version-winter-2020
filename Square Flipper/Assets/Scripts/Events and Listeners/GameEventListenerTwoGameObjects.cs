﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class GameEventListenerTwoGameObjects : MonoBehaviour
{
    [SerializeField] private GameEventTwoGameObjects _gameEvent;
    [SerializeField] private UnityEventTwoGameObjects _onInvoke;

    private void OnEnable()
    {
        _gameEvent.RegisterListener(this);
    }

    private void OnDisable()
    {
        _gameEvent.UnregisterListener(this);
    }

    public void Raise(GameObject go1, GameObject go2)
    {
        if (_onInvoke != null)
            _onInvoke.Invoke(go1, go2);
    }
}

