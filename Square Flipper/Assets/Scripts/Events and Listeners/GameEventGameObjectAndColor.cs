﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New GameObjectAndColor Event", menuName = "Game Events/New GameObjectAndColor Event")]
public class GameEventGameObjectAndColor : ScriptableObject
{
    private List<GameEventListenerGameObjectAndColor> _registeredListeners = new List<GameEventListenerGameObjectAndColor>();

    public void Raise(GameObject go1, Color color)
    {
        foreach (GameEventListenerGameObjectAndColor listener in _registeredListeners)
        {
            listener.Raise(go1, color);
        }
    }

    public void RegisterListener(GameEventListenerGameObjectAndColor listener)
    {
        if (!_registeredListeners.Contains(listener))
            _registeredListeners.Add(listener);
    }

    public void UnregisterListener(GameEventListenerGameObjectAndColor listener)
    {
        if (_registeredListeners.Contains(listener))
            _registeredListeners.Remove(listener);
    }
}
