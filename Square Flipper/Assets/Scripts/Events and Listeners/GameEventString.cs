﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New String Event", menuName = "Game Events/New String Event")]
public class GameEventString : ScriptableObject
{
    private List<GameEventListenerString> _registeredListeners = new List<GameEventListenerString>();

    public void Raise(string name)
    {
        foreach (GameEventListenerString listener in _registeredListeners)
        {
            listener.Raise(name);
        }
    }

    public void RegisterListener(GameEventListenerString listener)
    {
        if (!_registeredListeners.Contains(listener))
            _registeredListeners.Add(listener);
    }

    public void UnregisterListener(GameEventListenerString listener)
    {
        if (_registeredListeners.Contains(listener))
            _registeredListeners.Remove(listener);
    }
}
