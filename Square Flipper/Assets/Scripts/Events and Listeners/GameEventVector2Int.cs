﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Board Dimensions Event", menuName = "Game Events/New Board Dimensions Event")]
public class GameEventVector2Int : ScriptableObject
{
    [SerializeField] Vector2Int dimensions;

    private List<GameEventVector2IntListener> _registeredListeners = new List<GameEventVector2IntListener>();

    public void Raise(Vector2Int dimensionsOther)
    {
        foreach (GameEventVector2IntListener listener in _registeredListeners)
        {
            listener.Raise(dimensionsOther);
        }
    }

    public void Raise()
    {
        foreach (GameEventVector2IntListener listener in _registeredListeners)
        {
            listener.Raise(dimensions);
        }
    }

    public void RegisterListener(GameEventVector2IntListener listener)
    {
        if (!_registeredListeners.Contains(listener))
            _registeredListeners.Add(listener);
    }

    public void UnregisterListener(GameEventVector2IntListener listener)
    {
        if (_registeredListeners.Contains(listener))
            _registeredListeners.Remove(listener);

    }
}
