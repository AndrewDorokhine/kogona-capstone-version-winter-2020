﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class GameEventListenerGameObjectAndColor : MonoBehaviour
{
    [SerializeField] private GameEventGameObjectAndColor _gameEvent;
    [SerializeField] private UnityEventGameObjectAndColor _onInvoke;

    private void OnEnable()
    {
        _gameEvent.RegisterListener(this);
    }

    private void OnDisable()
    {
        _gameEvent.UnregisterListener(this);
    }

    public void Raise(GameObject go1, Color color)
    {
        if (_onInvoke != null)
            _onInvoke.Invoke(go1, color);
    }
}
