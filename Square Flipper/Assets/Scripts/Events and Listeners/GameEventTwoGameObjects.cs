﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Square Wiggler Event", menuName = "Game Events/New Square Wiggler Event")]
public class GameEventTwoGameObjects : ScriptableObject
{
    private List<GameEventListenerTwoGameObjects> _registeredListeners = new List<GameEventListenerTwoGameObjects>();

    public void Raise(GameObject go1, GameObject go2)
    {
        foreach (GameEventListenerTwoGameObjects listener in _registeredListeners)
        {
            listener.Raise(go1, go2);
        }
    }

    public void RegisterListener(GameEventListenerTwoGameObjects listener)
    {
        if (!_registeredListeners.Contains(listener))
            _registeredListeners.Add(listener);
    }

    public void UnregisterListener(GameEventListenerTwoGameObjects listener)
    {
        if (_registeredListeners.Contains(listener))
            _registeredListeners.Remove(listener);
    }
}
