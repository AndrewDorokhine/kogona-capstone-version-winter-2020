﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckCollisions : MonoBehaviour
{
    [SerializeField] private GameEvent _squareMatchedEvent;
    [SerializeField] private GameEventTwoGameObjects _squareWiggleOnMatchEvent;
    [SerializeField] private GameEvent _gameEndEvent;
	[SerializeField] private GameEvent _continueTutorialEvent;

	private MoveSquare _moveSquare;

    /* By creating small colliders as children of the squares, the squares won't
     * register collisions just be being placed close to each other. The player
     * will have to actually flip them.
     * 
     * One problem with this method is that every square prefab has its own collider
     * and they all bear this script: thus, every collision will activate OnTriggerEnter()
     * twice. Worked around by a boolean checking that a square was touched by the player.
     * Still causes some problems if squares are moved into each other by a very quick player.    
     */
    void Start()
    {
        _moveSquare = GetComponentInParent<MoveSquare>();
    }

    private void OnTriggerEnter(Collider other)
    {
        _continueTutorialEvent.Raise(); // For tutorial to continue only after the user does the requested action

        if (other.CompareTag("Collider"))
        {
            if (gameObject.GetComponentInParent<MeshRenderer>().material.color == other.GetComponentInParent<MeshRenderer>().material.color)
            {
                if (_moveSquare.WasSquareDragged())
                {
                    _squareMatchedEvent.Raise();
                    _squareWiggleOnMatchEvent.Raise(gameObject.transform.parent.gameObject, other.transform.parent.gameObject);
                    //All squares have 2 children - their mesh borders and the collider
                    //If a powerup is present it will have 3 children, running this if statement.
                    if (gameObject.transform.parent.childCount > 2)
                    {
                        // This must be called through GetComponent instead of an event, as all icons on screen will react to an event call.
                        // This causes incorrect behaviour if more than one icon is on the screen.
                        gameObject.transform.parent.GetComponentInChildren<PowerupBehaviour>().TransferIconToNewParent(other.transform.parent.gameObject);
                        //_transferRandomizerIconEvent.Raise(other.transform.parent.gameObject);
                    }
                }

                _moveSquare.EndDragging();
            }
            else
            {
                _gameEndEvent.Raise();
            }
        }
    }
}
