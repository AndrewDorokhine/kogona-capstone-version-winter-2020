﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening; // Use for tweening the rotation, so you're not using animations.

public class MoveSquare : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    [SerializeField] private GameEvent _stopFlashingTutorialSquares;
    [SerializeField] private BoolVariable _isButtonUsable; // Will disable the randomizer button while squares are moving, so square colours don't change mid-flip.
    [SerializeField] private IntVariable _swipeDirectionChangedThreshold;
    [SerializeField] private float flipSpeed;

    private Camera _camera;
    private Transform _rightUpPivot;
    private Transform _leftDownPivot;
    private Vector2 _startingSwipeDirection;
    private Vector2 _currentSwipeDirection;
    private Vector2 _currentSwipeDirectionNonNormalized;
    private Vector3 _startingSquareScale;

    private bool _canDrag = true;
    private bool _wasDragged = false;
    private bool _isWiggleTweenRunning = false; // Backing store
    public bool IsWiggleTweenRunning
    {
        get
        {
            return _isWiggleTweenRunning;
        }
    }

    private bool _defaultCanDragRight;
    private bool _defaultCanDragUp;
    private bool _defaultCanDragLeft;
    private bool _defaultCanDragDown;

    private bool _canDragRight;
    private bool _canDragUp;
    private bool _canDragLeft;
    private bool _canDragDown;

    private float _timeDragHeldTimer;

    void Start()
    {
        _startingSquareScale = gameObject.transform.localScale;
        ResetDraggingBools(); // Added for testing, did not exist previously
        _isButtonUsable.value = true;
        _camera = Camera.main;
        _leftDownPivot = gameObject.transform.parent;
        _rightUpPivot = _leftDownPivot.transform.parent;
        Input.multiTouchEnabled = true; // Reenabling multitouch for testing. Was previously false.
    }

    void Update()
    {
        _timeDragHeldTimer += Time.deltaTime;
    }

    // By setting these "defaults," ResetDraggingBools() can make sure that the squares are consistent in how they're allowed to move. Squares on the left side can never be moved left, for example.
    public void setDefaultValidRotations(bool right, bool up, bool left, bool down)
    {
        _defaultCanDragRight = right;
        _defaultCanDragUp = up;
        _defaultCanDragLeft = left;
        _defaultCanDragDown = down;
}

    public void OnBeginDrag(PointerEventData eventData)
    {
        _canDrag = true;
        _wasDragged = true;
        _startingSwipeDirection = (eventData.position - eventData.pressPosition);
        _timeDragHeldTimer = 0f;
        _stopFlashingTutorialSquares.Raise();
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (_canDrag)
        {
            // normalizing swipe direction for different screen sizes
            _currentSwipeDirection = (eventData.delta).normalized;
            _currentSwipeDirectionNonNormalized = (eventData.delta);

            if (Mathf.Abs(_startingSwipeDirection.x) > Mathf.Abs(_startingSwipeDirection.y))
            {
                // Swap + and - in the calculations to "swap" which hinge is moving (i.e. flipping left or right)
                if (_startingSwipeDirection.x > 0 && _canDragRight)
                {
                    _canDragUp = false;
                    _canDragLeft = false;
                    _canDragDown = false;

                    _rightUpPivot.Rotate(Vector3.down, _currentSwipeDirection.x * (Time.deltaTime * 250));
                    if (_rightUpPivot.rotation.y > 0)
                    {
                        _rightUpPivot.rotation = new Quaternion(0, 0, 0, 0);
                    }
                    if (_rightUpPivot.rotation.y < -0.99f)
                    {
                        EndDragging();
                    }
                }
                else if (_startingSwipeDirection.x < 0 && _canDragLeft)
                {
                    _canDragUp = false;
                    _canDragRight = false;
                    _canDragDown = false;

                    _leftDownPivot.Rotate(Vector3.down, _currentSwipeDirection.x * (Time.deltaTime * 250));
                    if (_leftDownPivot.rotation.y < 0)
                    {
                        _leftDownPivot.rotation = new Quaternion(0, 0, 0, 0);
                    }
                    if (_leftDownPivot.rotation.y > 0.99f)
                    {
                        EndDragging();
                    }
                }
            }

            if (Mathf.Abs(_startingSwipeDirection.x) < Mathf.Abs(_startingSwipeDirection.y))
            {
                if (_startingSwipeDirection.y > 0 && _canDragUp)
                {
                    _canDragLeft = false;
                    _canDragRight = false;
                    _canDragDown = false;

                    _rightUpPivot.Rotate(Vector3.right, _currentSwipeDirection.y * (Time.deltaTime * 250));
                    if (_rightUpPivot.rotation.x < 0)
                    {
                        _rightUpPivot.rotation = new Quaternion(0, 0, 0, 0);
                    }
                    if(_rightUpPivot.rotation.x > 0.99f)
                    {
                        EndDragging();
                    }
                }

                else if (_startingSwipeDirection.y < 0 && _canDragDown)
                {
                    _canDragLeft = false;
                    _canDragRight = false;
                    _canDragUp = false;

                    _leftDownPivot.Rotate(Vector3.right, _currentSwipeDirection.y * (Time.deltaTime * 250));
                    if (_leftDownPivot.rotation.x > 0)
                    {
                        _leftDownPivot.rotation = new Quaternion(0, 0, 0, 0);
                    }
                    if (_leftDownPivot.rotation.x < -0.99f)
                    {
                        EndDragging();
                    }
                }

            }
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Vector3 targetRotation = Vector3.zero;
        Transform pivot = null;
        _isButtonUsable.value = false; // While square is flipping with DOTween, randomizer button cannot be used.

        // This big block is to refine movement by reducing accidental/bad swipes.
        // Kinda massive though, see if its size can be reduced at all.
        if (_canDragRight || _canDragLeft)
        {
            if (_currentSwipeDirectionNonNormalized.x > 0 && _startingSwipeDirection.x < 0)
            {
                ResetPos();
                ResetDraggingBools();

                return;
            }

            else if (_currentSwipeDirectionNonNormalized.x < 0 && _startingSwipeDirection.x > 0)
            {
                ResetPos();
                ResetDraggingBools();

                return;
            }
        }
        else if (_canDragUp || _canDragDown)
        {
            if (_currentSwipeDirectionNonNormalized.y > 0 && _startingSwipeDirection.y < 0)
            {
                ResetPos();
                ResetDraggingBools();

                return;
            }

            else if (_currentSwipeDirectionNonNormalized.y < 0 && _startingSwipeDirection.y > 0)
            {
                ResetPos();
                ResetDraggingBools();

                return;
            }
        }

        if (_canDragRight || _canDragLeft)
        {
            if (Mathf.Abs(_currentSwipeDirectionNonNormalized.x) < Mathf.Abs(_startingSwipeDirection.y))
            {
                ResetPos();
                ResetDraggingBools();

                return;
            }
        }

        else if (_canDragUp || _canDragDown)
        {
            if (Mathf.Abs(_currentSwipeDirectionNonNormalized.y) < Mathf.Abs(_startingSwipeDirection.x))
            {
                ResetPos();
                ResetDraggingBools();

                return;
            }
        }

        if (_canDragRight)
        {
            if (Mathf.Abs(eventData.position.x) < Mathf.Abs(eventData.pressPosition.x + _swipeDirectionChangedThreshold.value))
            {
                ResetPos();
                ResetDraggingBools();

                return;
            }
        }

        else if (_canDragLeft)
        {
            if (Mathf.Abs(eventData.position.x) > Mathf.Abs(eventData.pressPosition.x - _swipeDirectionChangedThreshold.value))
            {
                ResetPos();
                ResetDraggingBools();

                return;
            }
        }

        else if (_canDragUp)
        {
            if (Mathf.Abs(eventData.position.y) < Mathf.Abs(eventData.pressPosition.y + _swipeDirectionChangedThreshold.value))
            {
                ResetPos();
                ResetDraggingBools();

                return;
            }
        }

        else if (_canDragDown)
        {
            if (Mathf.Abs(eventData.position.y) > Mathf.Abs(eventData.pressPosition.y - _swipeDirectionChangedThreshold.value))
            {
                ResetPos();
                ResetDraggingBools();

                return;
            }
        }

        if (_timeDragHeldTimer >= 0.5f)
        {

            if (_canDragRight && _rightUpPivot.rotation.y <= -0.7f)
            {
                pivot = _rightUpPivot;
                targetRotation = new Vector3(0, -180, 0);
            }
            else if (_canDragLeft && _leftDownPivot.rotation.y >= 0.7f)
            {
                pivot = _leftDownPivot;
                targetRotation = new Vector3(0, 180, 0);
            }

            else if (_canDragUp && _rightUpPivot.rotation.x >= 0.7f)
            {
                pivot = _rightUpPivot;
                targetRotation = new Vector3(180, 0, 0);
            }

            else if (_canDragDown && _leftDownPivot.rotation.x <= -0.7f)
            {
                pivot = _leftDownPivot;
                targetRotation = new Vector3(-180, 0, 0);
            }

            if (pivot != null)
            {
                FlipSquare(pivot, targetRotation);
            }
            else
            {
                ResetPos();
            }
        }
        else
        {
            if (_canDragRight)
            {
                pivot = _rightUpPivot;
                targetRotation = new Vector3(0, -180, 0);
            }
            else if (_canDragLeft)
            {
                pivot = _leftDownPivot;
                targetRotation = new Vector3(0, 180, 0);
            }
            else if (_canDragUp)
            {
                pivot = _rightUpPivot;
                targetRotation = new Vector3(180, 0, 0);
            }
            else if (_canDragDown)
            {
                pivot = _leftDownPivot;
                targetRotation = new Vector3(-180, 0, 0);
            }

            if (pivot != null)
            {
                FlipSquare(pivot, targetRotation);
            }
            else
            {
                ResetPos();
            }
        }

        ResetDraggingBools();
    }

    private void FlipSquare(Transform pivot, Vector3 targetRotation)
    {
        Sequence flipSequence = DOTween.Sequence();

        Quaternion rotation = Quaternion.Euler(targetRotation);

        flipSequence.Append(pivot.DORotateQuaternion(rotation, flipSpeed)).OnComplete(ResetPos);
    }

    private void ResetDraggingBools()
    {
        _canDragRight = _defaultCanDragRight;
        _canDragUp = _defaultCanDragUp;
        _canDragLeft = _defaultCanDragLeft;
        _canDragDown = _defaultCanDragDown;
       
        _canDrag = true;
    }

    // Allows the game to force the player to drop their square after they make a match.
    public bool EndDragging()
    {
        ResetPos();
        return _canDrag = false;
    }

    /* Instead of spending processing power destroying and building squares, it may
     * be possible to reuse those that already exist and simply change their
     * color to make them "new squares." A public function for resetting positions would
     * be valuable in that case.
     */
    public void ResetPos()
    {
        _rightUpPivot.rotation = new Quaternion(0, 0, 0, 0);
        _leftDownPivot.rotation = new Quaternion(0, 0, 0, 0);

        gameObject.transform.rotation = new Quaternion(0, 0, 0, 0);

        _wasDragged = false;
        _isButtonUsable.value = true; // Reenable randomizer button when squares reset.
    }

    public bool WasSquareDragged()
    {
        return _wasDragged;
    }

    // Make the square wiggle when randomized, unless it's in the middle of a wiggle - initiated from DOTweeningBehaviour
    public void RunWiggleTween(float amountToPunch, int strength, int vibrato, float duration)
    {
        if (!IsWiggleTweenRunning)
        {
            WiggleTweenNowRunning();
            Sequence squareWiggleSequence = DOTween.Sequence();
            squareWiggleSequence.Append(gameObject.transform.DOPunchScale(new Vector3(amountToPunch, amountToPunch, amountToPunch), strength, vibrato, duration).OnComplete(WiggleTweenHasEnded));
            squareWiggleSequence.Append(gameObject.transform.DOScale(new Vector3(_startingSquareScale.x, _startingSquareScale.y, _startingSquareScale.z), 0));
        }
    }

    // Prevents the square from having the wiggle tween applied more than once when the board randomizes.
    public void WiggleTweenNowRunning()
    {
        _isWiggleTweenRunning = true;
    }

    public void WiggleTweenHasEnded()
    {
        _isWiggleTweenRunning = false;
    }

    // Tutorial functions so player must do as the tutorial says to continue.
    public void EnableUpDragForTutorial()
    {
        _defaultCanDragUp = true;
        _canDragUp = true;
    }

    public void EnableDownDragForTutorial()
    {
        _defaultCanDragDown = true;
        _canDragDown = true;
    }

    public void DisableTutorialDragging()
    {
        _defaultCanDragUp = false;
        _defaultCanDragDown = false;
        ResetDraggingBools();
    }
}